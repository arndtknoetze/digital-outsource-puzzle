import { Component } from '@angular/core';
import { Game } from './game';
import { timer } from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {
  title = 'puzzle';
  game_settings = {
    blocks: 16
  }
  blocks : Game[] = [];
  status = false;
  time = null;
  count = 0;

  ngOnInit() {
    console.log('init')
    for (let i = 1; i <= this.game_settings.blocks; i++) {
      this.blocks.push({
        number: i,
        background: 'image_part_00'+i,
        status: i==1 ? 'blank' : ''
      })
    }
    this.blocks = this.shuffle(this.blocks)
    
  }


  timer() {
    timer(0, 1000).subscribe(ellapsedCycles => {
        this.time++;
    });
  }

  checkComplete(blocks) {
    let count = 1
    for (let i = 0; i < blocks.length; i++) {

     if(blocks[i].number == count) {
      count++
     } else {
       break
     }

     if(blocks.length == count) {
      this.status = true
     }
    }
  }

  playAgain(){
    this.blocks = [];
    for (let i = 1; i <= this.game_settings.blocks; i++) {
      this.blocks.push({
        number: i,
        background: 'image_part_00'+i,
        status: i==1 ? 'blank' : ''
      })
    }
    this.blocks = this.shuffle(this.blocks)
  }

  shuffle(array) {
    this.status = false
    this.count = 0
    var currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  move(block_val) {
    let click_Block = this.blocks[(block_val)]
    this.timer()
    // RIght
    if(this.blocks[(block_val+1)] && this.blocks[(block_val+1)]['status'] === 'blank') {
      if(block_val+1 == 4 || block_val+1 == 8 || block_val+1 == 12){ 
      } else {
        this.blocks[(block_val)] = this.blocks[(block_val+1)]
        this.blocks[(block_val+1)] = click_Block
        this.count = this.count+1
      }
    }

    // Left
    if(this.blocks[(block_val-1)] && this.blocks[(block_val-1)]['status'] === 'blank') {
      if(block_val-1 == 3 || block_val-1 == 7 || block_val-1 == 11){ 
      } else {
        this.blocks[(block_val)] = this.blocks[(block_val-1)]
        this.blocks[(block_val-1)] = click_Block
        this.count = this.count+1
      }
    }

    // Bottom
    if(this.blocks[(block_val+4)] && this.blocks[(block_val+4)]['status'] === 'blank') {
      this.blocks[(block_val)] = this.blocks[(block_val+4)]
      this.blocks[(block_val+4)] = click_Block
      this.count = this.count+1
    }

    // Top
    if(this.blocks[(block_val-4)] && this.blocks[(block_val-4)]['status'] === 'blank') {
      this.blocks[(block_val)] = this.blocks[(block_val-4)]
      this.blocks[(block_val-4)] = click_Block
      this.count = this.count+1
    }

    this.checkComplete(this.blocks)
  }
  
}

